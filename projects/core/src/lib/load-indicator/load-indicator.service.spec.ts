import {fakeAsync, TestBed} from '@angular/core/testing';
import { LoadIndicatorService } from './load-indicator.service';

describe('LoadIndicatorService', () => {
  let service: LoadIndicatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadIndicatorService);
  });

  afterEach(() => {
    service.clear(); // Reset state after each test
    jest.useRealTimers(); // Reset timers if mocked
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should start with an empty process list and shouldShow as false', () => {
    expect(service.getList()).toEqual([]);
    expect(service.shouldShow()).toBe(false);
  });

  it('should add a process to the list and set shouldShow to true', () => {
    const result = service.push('process1');
    expect(result).toBe(true);
    expect(service.getList()).toEqual(['process1']);
    expect(service.shouldShow()).toBe(true);
  });

  it('should not add a duplicate process', () => {
    service.push('process1');
    const result = service.push('process1');
    expect(result).toBe(false);
    expect(service.getList()).toEqual(['process1']);
  });

  it('should remove a process from the list and update shouldShow', () => {
    service.push('process1');
    service.pop('process1');
    expect(service.getList()).toEqual([]);
    expect(service.shouldShow()).toBe(false);
  });

  it('should handle removing a process that does not exist', () => {
    service.push('process1');
    service.pop('process2');
    expect(service.getList()).toEqual(['process1']);
    expect(service.shouldShow()).toBe(true);
  });

  it('should clear the process list and set shouldShow to false', () => {
    service.push('process1');
    service.push('process2');
    service.clear();
    expect(service.getList()).toEqual([]);
    expect(service.shouldShow()).toBe(false);
  });

  it('should trigger the watchdog timer and clear the list after timeout', fakeAsync(() => {
    jest.useFakeTimers();
    service.push('process1');
    expect(service.shouldShow()).toBe(true);
    jest.advanceTimersByTime(30000); // Fast-forward time
    expect(service.getList()).toEqual([]);
    expect(service.shouldShow()).toBe(false);
  }));

  it('should reset the watchdog timer when a new process is added', fakeAsync(() => {
    jest.useFakeTimers();
    service.push('process1');
    jest.advanceTimersByTime(15000); // Advance halfway
    service.push('process2'); // Add a new process
    jest.advanceTimersByTime(15000); // Time reset
    expect(service.getList()).toEqual(['process1', 'process2']);
    jest.advanceTimersByTime(15000); // Advance again
    expect(service.getList()).toEqual([]);
    expect(service.shouldShow()).toBe(false);
  }));

  it('should clear the watchdog timer on destroy', () => {
    jest.useFakeTimers();
    service.push('process1');
    service.ngOnDestroy();
    expect(() => jest.advanceTimersByTime(30000)).not.toThrow(); // Should not trigger timeout
    expect(service.getList()).toEqual([]);
    expect(service.shouldShow()).toBe(false);
  });
});
