import { Component, OnInit } from '@angular/core';
import {AlertService} from '@ratespecial/core';

@Component({
  selector: 'app-alert-test',
  templateUrl: './alert-test.component.html',
  styleUrls: ['./alert-test.component.scss']
})
export class AlertTestComponent implements OnInit {

  constructor(private alertService: AlertService) { }

  ngOnInit(): void {
  }

  success(): void {
    this.alertService.success('A success message');
  }

  warning(): void {
    this.alertService.warning('A warning message');
  }

  error(): void {
    this.alertService.error('A error message');
  }

  forever(): void {
    this.alertService.addAlert({type: 'success', msg: 'You have to manually close this', timeout: 0});
  }
}
