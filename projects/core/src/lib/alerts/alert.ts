export interface Alert {
  id?: number;
  type: AlertType;
  msg: string;
  /**
   * How long in milliseconds the alert should remain open before auto closing
   */
  timeout?: number;
  /**
   * Add to the collection of alerts within AlertService or not
   */
  scoped?: boolean;
  close?: (alerts: Alert[]) => void;
}

export declare type AlertType = 'success' | 'danger' | 'warning' | 'info';
