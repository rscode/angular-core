import {Component, OnInit} from '@angular/core';
import {LoadIndicatorService} from '@ratespecial/core';

@Component({
  selector: 'app-load-indicator-test',
  templateUrl: './load-indicator-test.component.html',
  styleUrls: ['./load-indicator-test.component.scss']
})
export class LoadIndicatorTestComponent implements OnInit {

  tags: string[] = [];

  constructor(public loadIndicatorService: LoadIndicatorService) {
  }

  ngOnInit(): void {

  }

  push(): void {
    for (let x in [1,2,3]) {
      let tag = `load-process-${x}`;
      const added = this.loadIndicatorService.push(tag);

      if (added) {
        this.tags.push(tag);
      }

    }

    console.log('pushed to indicator service');
  }

  pop(): void {
    const randomIndex = Math.floor(Math.random() * this.tags.length);
    const tag = this.tags.splice(randomIndex, 1)[0];


    this.loadIndicatorService.pop(tag);
    console.log(`pop from indicator service: ${tag}`);
  }
}
