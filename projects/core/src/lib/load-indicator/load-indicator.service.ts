import {effect, Injectable, OnDestroy, signal} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadIndicatorService implements OnDestroy {

  private pList: string[] = [];

  shouldShow = signal<boolean>(false);

  private watchdogTimeout = 30000;
  private watchdogSubscription: any = null;

  constructor() {
    this.setupWatchdog();
  }

  ngOnDestroy(): void {
    // Resets watchdog subscription
    this.clear();
  }

  push(procName: string): boolean {
    // Only add if not in list
    if (this.pList.indexOf(procName) === -1) {
      this.pList.push(procName);
      this.notify();
      return true;
    }

    return false;
  }

  pop(procName: string): void {
    const index = this.pList.indexOf(procName);

    if (index !== -1) {
      this.pList.splice(index, 1);
    }

    this.notify();
  }

  getList(): readonly string[] {
    return this.pList;
  }

  clear(): void {
    this.pList = [];
    this.notify();

    if (this.watchdogSubscription) {
      clearTimeout(this.watchdogSubscription);
    }
  }

  protected notify(): void {
    this.shouldShow.set(this.pList.length > 0);
  }

  /**
   * Manage a watchdog timer to make sure we don't have a load indicator that displays forever.
   * @protected
   */
  protected setupWatchdog(): void {
    effect(() => {
      if (this.shouldShow()) {
        if (this.watchdogSubscription) {
          clearTimeout(this.watchdogSubscription);
        }

        this.watchdogSubscription = setTimeout(() => this.clear(), this.watchdogTimeout);
      }
    });
  }
}
