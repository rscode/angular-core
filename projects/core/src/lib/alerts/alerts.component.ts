import {Component, OnDestroy, OnInit} from '@angular/core';
import {AlertService} from './alert.service';
import {Alert} from './alert';
import {NgClass, NgForOf, NgIf} from '@angular/common';
import {NgbAlert} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'rs-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss'],
  standalone: true,
  imports: [
    NgClass,
    NgForOf,
    NgbAlert,
    NgIf,
  ],
})
export class AlertsComponent implements OnInit, OnDestroy {

  alerts: Alert[];

  constructor(private alertService: AlertService) {
  }

  ngOnInit(): void {
    this.refresh();
  }

  refresh(): void {
    this.alerts = this.alertService.get();
  }

  ngOnDestroy(): void {
    this.alerts = [];
  }

}
