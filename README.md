# RS Angular Core

The official Angular library development guide was used to build this: https://angular.io/guide/creating-libraries#getting-started

This package was structured similarly to [ng-bootstrap](https://github.com/ng-bootstrap/ng-bootstrap).  They use one package with many
modules inside.

## Testing the library in this repo

1. Build the library in watch mode: `npm run watch`
1. Start test application using `npm run start`

When you try and use any module / component / service from the library in the test application, be sure
it references the package as if it was installed through node_modules, and NOT a relative path:

```ts
import {LoadIndicatorService} from '@ratespecial/core';
```

This works because `tsconfig.json` should point to the correct `dist` folder:

```json
    "paths": {
      "core": [
        "dist/core/core",
        "dist/core"
      ]
    },
```

## Testing the library in another project 

Consider [yarn link](https://docs.npmjs.com/cli/v7/commands/npm-link).  This needs to be done from the 
built `dist/core` folder.

## Code scaffolding

According to `angular.json`, the `defaultProject` is set to `core` which has all the components.  Therefore you can generate new
components as usual

```
ng generate component|directive|pipe|service|class|guard|interface|enum|module component-name
```

To generate a new component in lib-test project, you must specify it

```
ng generate component lib-test --project=lib-test
```

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

1. Bump the `projects/core/package.json` version
1. `npm run build`
1. `cd dist/core`
1. `npm publish --access public`
