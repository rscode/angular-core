import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {LoadIndicatorService} from '../load-indicator.service';
import {LoadIndicatorStyle} from '../load-indicator-style.enum';
import {CommonModule} from '@angular/common';

@Component({
  selector: 'rs-load-indicator',
  templateUrl: 'load-indicator.component.html',
  styleUrls: ['load-indicator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    CommonModule,
  ],
})
export class LoadIndicatorComponent {
  /**
   * Will show a message fading in and out while the load indicator is shown.
   */
  @Input() pulseMessage = '';
  /**
   * What the load indicator will look like
   */
  @Input() iconStyle = LoadIndicatorStyle.CIRCLE;
  /**
   * Should it be floating in the middle of the page, or should it be bounded to the parent
   */
  @Input() floating = true;

  constructor(public loadIndicatorService: LoadIndicatorService) {
  }
}
