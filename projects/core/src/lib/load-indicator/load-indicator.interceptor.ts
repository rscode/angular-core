import {tap} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoadIndicatorService} from './load-indicator.service';

/**
 * Include in a module:
 * @NgModule({
 *   providers: [
 *     {
 *       provide: HTTP_INTERCEPTORS,
 *       useClass: LoadIndicatorInterceptor,
 *       multi: true,
 *     }
 *   ]
 * })
 */

@Injectable()
export class LoadIndicatorInterceptor implements HttpInterceptor {

  constructor(private loadIndicatorService: LoadIndicatorService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const key = req.urlWithParams;

    this.loadIndicatorService.push(key);

    return next
      .handle(req).pipe(
        tap({
          next: resp => {
            if (resp instanceof HttpResponse) {
              this.loadIndicatorService.pop(key);
            }
          },
          error: () => {
            this.loadIndicatorService.pop(key);
          },
        }),
      );
  }
}
