export interface ApiError {
  code: number;
  title: string;
  detail: string;
}
