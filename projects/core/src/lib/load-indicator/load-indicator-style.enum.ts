export enum LoadIndicatorStyle {
  CIRCLE = 'circle',
  CUBE = 'cube',
}
