import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {LoadIndicatorTestComponent} from './load-indicator-test/load-indicator-test.component';
import {AlertTestComponent} from './alert-test/alert-test.component';
import { LoadIndicatorInlineComponent } from './load-indicator-test-inline/load-indicator-inline.component';
import {AlertsComponent, LoadIndicatorComponent} from '@ratespecial/core';

@NgModule({
  declarations: [
    AppComponent,
    LoadIndicatorTestComponent,
    AlertTestComponent,
    LoadIndicatorInlineComponent
  ],
  imports: [
    BrowserModule,
    AlertsComponent,
    LoadIndicatorComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
