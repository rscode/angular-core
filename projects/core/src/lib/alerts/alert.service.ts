import {Injectable, SecurityContext} from '@angular/core';
import {Alert} from './alert';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiError} from './api-error.model';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private defaultSuccessTimeout = 4000;
  private defaultTimeout = 8000;

  private alertId = 0;
  private alerts: Alert[] = [];

  /**
   * Message Replacement Map for replacing Vendor Error Messages with more user friendly messaging
   */
  private messageMap = {
    token_not_provided: 'Your session has expired.  Please log in.',
    token_expired: 'Your session has expired.  Please log in.',
    token_invalid: 'Your session has expired.  Please log in.'
  };


  constructor(private sanitizer: DomSanitizer) {
  }

  clear(): void {
    this.alerts.splice(0, this.alerts.length);
  }

  get(): Alert[] {
    return this.alerts;
  }

  success(msg: string): void {
    this.addAlert({
      type: 'success',
      msg,
      timeout: this.defaultSuccessTimeout,
    });
  }

  error(msg: string): void {
    this.addAlert({
      type: 'danger',
      msg,
      timeout: this.defaultTimeout,
    });
  }

  warning(msg: string): void {
    this.addAlert({
      type: 'warning',
      msg,
      timeout: this.defaultTimeout,
    });
  }

  info(msg: string): void {
    this.addAlert({
      type: 'info',
      msg,
      timeout: this.defaultTimeout,
    });
  }

  addAlert(alertOptions: Alert, extAlerts?: Alert[]): void {

    if (this.messageMap.hasOwnProperty(alertOptions.msg)) {
      alertOptions.msg = this.messageMap[alertOptions.msg];
    }

    if (this.isDuplicate(alertOptions) === false) {
      alertOptions.id = this.alertId++;

      this.factory(alertOptions);

      if (alertOptions.timeout && alertOptions.timeout > 0) {
        setTimeout(() => {
          this.closeAlert(alertOptions.id, extAlerts);
        }, alertOptions.timeout);
      }
    }
  }

  private factory(alertOptions: Alert): Alert {

    const alert: Alert = {
      type: alertOptions.type,
      msg: this.sanitizer.sanitize(SecurityContext.HTML, alertOptions.msg),
      id: alertOptions.id,
      timeout: alertOptions.timeout,
      scoped: alertOptions.scoped,
      close: (alerts) => this.closeAlert(alertOptions.id, alerts),
    };

    if (!alert.scoped) {
      this.alerts.push(alert);
    }

    return alert;
  }

  /**
   * Check for duplicate messages
   */
  private isDuplicate(alertOptions: Alert): boolean {

    let isDuplicate = false;

    this.alerts.map(alert => {
      if (alertOptions.msg === alert.msg) {
        isDuplicate = true;
      }
    });

    return isDuplicate;
  }

  addApiError(apiError: ApiError): void {
    const error = '<strong>' + apiError.code + ' ' + apiError.title + '</strong>: ' + apiError.detail;
    this.error(error);
  }

  handleApiErrors(err): void {
    if (err.error.hasOwnProperty('errors')) {
      err.error.errors.map(error => {
        // Are the errors an array of API Error Objects or an array of strings
        if (typeof error === 'string') {
          this.error(error);
        } else {
          this.addApiError(error);
        }
      });
    } else if (typeof err.error === 'object') {
      this.addApiError(err.error);
    } else {
      this.error(err.error);
    }
  }

  closeAlert(id: number, extAlerts?: Alert[]): any {
    const thisAlerts = (extAlerts && extAlerts.length > 0) ? extAlerts : this.alerts;
    const index = thisAlerts.map((e) => e.id).indexOf(id);

    return this.closeAlertByIndex(index, thisAlerts);
  }

  closeAlertByIndex(index: number, thisAlerts: Alert[]): Alert[] {
    return thisAlerts.splice(index, 1);
  }

}
