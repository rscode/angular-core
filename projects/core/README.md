# Angular Core

Provides common components used in the majority of our Angular sites.

## Using the library

```
yarn add @ratespecial/core
```

All components are stand alone, and all services are provided in root.  Use what you need, where you need it.
Include a specific module if you only need certain functionality, or the core module if you would like the whole package


For detailed documentation on each component, [see the wiki](https://bitbucket.org/rscode/angular-core/wiki/Home)

## Running unit tests

Run `ng test core` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
