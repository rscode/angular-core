import {Component, OnInit} from '@angular/core';
import {LoadIndicatorService} from '@ratespecial/core';

/*
NOTE this provides its own LoadIndicatorService.  This is to not only test the floating=false property of the load indicator,
but also that it can work on it's own.
 */
@Component({
  selector: 'app-load-indicator-test-inline',
  templateUrl: './load-indicator-inline.component.html',
  styleUrls: ['./load-indicator-inline.component.scss'],
  providers: [LoadIndicatorService],
})
export class LoadIndicatorInlineComponent implements OnInit {

  constructor(private loadIndicatorService: LoadIndicatorService) {
  }

  ngOnInit(): void {
  }

  push(): void {
    this.loadIndicatorService.push('inline-process');
    console.log('pushed to indicator service');
  }

  pop(): void {
    this.loadIndicatorService.pop('inline-process');
    console.log('pop from indicator service');
  }
}
